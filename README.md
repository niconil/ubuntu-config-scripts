# Mode d'emploi

## Télécharger le projet
> git clone https://framagit.org/otyugh/ubuntu-config-scripts 

## Exécuter le script
Double-cliquer sur **ubuntuconfig.desktop**

## Aperçu
![Menu principal](https://gnuw.arzinfo.pw/permalink/04-2019/ubuntu-config-scripts-exemple-ver0.png)