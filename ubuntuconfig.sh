#!/bin/bash
set -x

###########
#variables#
###########
ownPathF="$(realpath "$0")"
ownPathD="$(dirname "$ownPathF")"
taskD="$ownPathD/task"
zWidth=600
zTitre="Préconfiguration Ubuntu"
nil='/dev/null'

######
#main#
######

#Being root with pkexec & workaround
if [ "$USER" != 'root' ]
then
	pkexec "$ownPathF" "$DISPLAY" "$XAUTHORITY"
	exit 1
else
	if [ -n "$1" ] && [ -n "$2" ]
	then
		export DISPLAY="$1"
		export XAUTHORITY="$2"
		export PKEXEC_HOME="/home/$(grep "$PKEXEC_UID:$PKEXEC_UID" '/etc/passwd' | sed 's|:.*$||')"
	elif [  -z "$DISPLAY" ] || [ -z "$XAUTHORITY" ]
	then
		echo "Pas d'environnement graphique en vue"
		exit 1
	fi
	export DEBIAN_FRONTEND='noninteractive'
fi

#Zenity files
out="$(mktemp)"
err="$(mktemp)"
function zerr() {
		echo "100" > "$out"
		zenity --error --width=$zWidth --title=$zTitre --text "$1\n\n$(cat "$err")" 2>"$nil" &
		exit 1
}

#Check for tasks
[ -d "$taskD" ] || zerr "$taskD devant contenir les tâches... Inexistant !"
tasks=($(find "$taskD" -type f -name "[^.]*" | sort -n))
[ ${#tasks[@]} -le 0 ] && zerr "Aucun script dans $taskD"

#Tasklist creation
list=()
for task in ${tasks[@]}
do
	source "$task" 2>"$err" || zerr "Plantage au chargement de $task"
	list+=("$check" "$task" "$description")
done

#GUI : picking
pick=$( \
zenity --list --checklist --hide-column 2 --height=$((${#tasks[@]}*50)) --width=$zWidth --title "$zTitre" \
--text 'Petit script de pre-configurations pour Ubuntu
<sub>Sont pré-cochées les options "toujours souhaitées" en général</sub>' \
--column '' --column 'Taskname' --column 'Tâche' "${list[@]}" 2>"$nil") || exit 0
[ -z "${pick[@]}" ] && exit 0
readarray -td'|' pick <<< "$pick"
[ ${#pick[@]} -eq 0 ] && exit 0

#GUI : do tasks
tail --pid=$$ -f "$out" 2>"$nil" | zenity --progress --auto-close --width=$zWidth --title "$zTitre" &
echo "#Initialisation" > "$out"

i=1
for task in ${pick[@]}
do
	source "$task" 2>"$err" || zerr "Plantage au chargement de $task"
	status="[$i/${#pick[@]}] $description"
	echo "#$status" >> "$out"
	sleep 1
	task 2>"$err" | sed -u "s|^|#$status\t|" 1>"$out"
	[ ${PIPESTATUS[0]} -ne 0 ] && zerr "Plantage à l'exécution de $task"
	echo "$(($i*100/${#pick[@]}))" >> "$out"
	let i++
done

echo "100" > "$out"
zenity --info --width=$zWidth --title "$zTitre" --text "Tout s'est bien déroulé !" 2>"$nil"
